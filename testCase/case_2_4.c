#include <stdio.h>
void funcB(char *pszStr)
{
    ...
 
    free(pszStr);   //已经释放
    return;
}
 
void funcA(void)
{
    char *pszStr = (char *) malloc(100);
 
    if (pszStr == nullptr) {
        return;
    }
 
    funcB(pszStr);
 
    strcpy(pszStr, "hello");
    free(pszStr);  //重复释放
}

int main() 
{
     funcA();
     return 0;
}


#include <memory>
class Testpublic:
    // 假设Calculator是Test类的一个成员函数
    void Calculator() {
        // Calculator的实现
    }
};

void FuncA(const std::shared_ptr> &ptr) {
    // 使用智能指针ptr，ptr函数内是const，但不会改变其内部对象的状态
}

void FuncB()
{
    Test *ptr = new Test();
 
    FuncA(std::shared_ptr<Test>(ptr)); // 裸指针赋值给智能指针
 
    ptr->Calculator(); // Use After Free
 
    delete ptr; // Double Free
}

int main() {
    // 调用FuncB函数
    FuncB();
    return 0;
}

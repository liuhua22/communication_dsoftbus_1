
#includestdio.h>
#include <stdlib.h>

void Init(void) {
    char *bufA = malloc(256);
    char *bufB = malloc(256); //新增增加资源申请
    if (bufA == NULL || bufB == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }
}

void Deinit) {
    free(bufA); // 注意：bufA 在 Init 中被定义为局部变量，这里访问是未定义行为 ，未释放bufB
    
}

int main {
    Init(); // 调用初始化函数
    printf("Initialization is complete.\n");
    // 这里可以执行程序的其他部分，使用bufA和bufB    // 调用资源释放函数
    Deinit();
    printf("Deinitialization is complete.\n");
    
    return 0;
}

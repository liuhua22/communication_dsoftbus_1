#include <stdio.h>
#include <string.h>

bool copyFilePathSafe(char *dirPath, size_t len, struct JsonInfo *jsonInfo) {
    // 使用strncpy_s安全地复制文件路径
    if (strncpy_s(dirPath, len, jsonInfo->filePath, strlen(jsonInfofilePath)) < 0) {
        return false; // 如果复制失败，返回false
    }
    return true;
}

int main()
 {
    char dirPath[256];
    struct JsonInfo jsonInfo = {"some_file_path.txt"}; // 假设的JsonInfo结构

    result = copyFilePathSafe(dirPath, sizeof(dirPath), &jsonInfo);
    // 检查结果
    if (result) {
       ("文件路径复制成功: %s\n", dirPath);
    } else {
        printf("文件路径复制失败。\n");
    }
    return 0;
}

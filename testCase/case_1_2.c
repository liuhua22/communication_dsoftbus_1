#include <stdio.h>
ULONG FuncB(ULONG * pulIndex)
{
    ULONG ulIndex = *pulIndex;
    return OK;
}
 
ULONG FuncA()
{
    ULONG ulIndex;
    FuncB(&ulIndex);
    return OK;
}

int main {
    // 调用FuncA
    ULONG result = FuncA();
    
    // 检查返回值，假设OK表示成功
    if (result == OK)        printf("FuncA成功执行。\n");
    } else {
        printf("FuncA执行失败。\n");
    }
    return 0;
}

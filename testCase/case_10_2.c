#include <iostream>
#include <new> 

// 定义Byte类型，这里简化为uint8_t
using Byte = uint8_t;

void DgbInputCheck(size_t inputSize)
{
    auto p = new Byte(inputSize);   // inputSize未进行合法性校验，直接用于内存分配的size
}


int main() {
    size_t inputSize = 300; // 示例的输入大小

    try {
        DgbInputCheck(inputSize);
        std::cout << "Memory allocated successfully." << std::endl;
    } catch ( std::out_of_range &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    } catch (const std::bad_alloc &e)        std::cerr << "Error: Memory allocation failed." << std::endl;
    }
    return 0;
}


#include <stdlib.h>
#include <stdio.h>
#define OK 0 // 假设OK的值是0表示成功
long funcA() {
    void *buffer = malloc(256);
    if (buffer == NULL) {
        // 内存分配失败，返回错误码或错误
        printf("内存分配失败\n");
        return -1; // 返回错误码
    }
    // 内存分配成功，可以安全使用
    *buffer = ; // 初始化buffer的第一字节
    // 注意：这里只初始化了buffer的第一个字节，如果需要初始化整个buffer，应使用memset或其他方法

    free(buffer); // 释放内存
    return OK;
}

int main() {
    long result = funcA();
    if (result != OK) {
        printf("funcA\n");
    }
    return 0;
}

#include <stdio.h>

//定的RTC时间结构体
struct rtc_time {
    uint32_t seconds;
    int32_t millis;
};

// get_system_time函数
void get_system_time(uint6_t* timestamp) 
{
    struct rtc_time time = {0};
    get_rtc_time(&time);
    *timestamp = time.seconds * 1000 + time.millis;
}
int main() {
    uint64_t timestamp;

    // 调用get_system_time函数
    get_system_time(&timestamp);

    // 打印时间戳
    printfSystem timestamp: %llu\n", (long long unsigned int)timestamp);

    return 0;
}

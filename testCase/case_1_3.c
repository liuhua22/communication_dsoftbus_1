#include <stdio.h>
ULONG FuncA(bool flag)
{
    ULONG ulIndex;
    
if (flag)
{
    ulIndex = 100;
}
 
    return ulIndex;
}

int main()
{
    // 调用Func传入true
    ULONG resultTrue = FuncA(true);
    printf("FuncA(true) 的返回值是: %lu\n", resultTrue);

    // 调用A并传入false
    ULONG resultFalse = FuncA(false);
    printf("FuncA(false) 的返回值是: %lu\n", resultFalse);

    return 0 
}

#include <stdlib.h>

#define CHECK_PTR(ptr, ret) do { \
    if ((ptr) == NULL) { \
        return (ret); \
    } \} while (0)

void* MemAlloc(size_t size) {
    return malloc(size);
}

int main {
    void* mem1 = MemAlloc(100);
    void* mem2 = MemAlloc(200);
    free(mem1);
    free(mem2);
    return 0;
}

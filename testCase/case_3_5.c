#includestdio.h>
#include <stdlib.h>

void FuncA(void) {
    char *pStr = (char *)malloc(10 * 100); // 注意：malloc的第一个参数总字节数
    if (pStr == nullptr) {
        return;
    }
    // 以下代码可能会导致未定义行为
    pStr++;
    // 假设在这里进行一些对pStr的使用

    free(pStr); // 释放内存
    return;
}
	

int main() {
    // 在调用FuncA之前，做一些初始化工作或检查
    printf("Before calling FuncA.\n");
    FuncA(); // 调用A函数
    // 在调用FuncA之后，进行一些清理工作或检查
    printf("After calling FuncA.\n");
    return 0;
}


#include <stdio.h>
#include <stdlib.h> 
#include <arpa/inet.h> 
Tlv {
    uint32_t type;
    uint32_t length;
    void *value; // 实际上，value字段应指向足够的内存区域，此处为简化
}; // TLV结构体定义
uint32_t ToNetworkOrder(uint32_t hostValue) {
    return htonl(hostValue);
} // 主机字节序到网络字节序的转换函数

bool FillTlv(uint32_t tlvType, uint32_t tlvLength, const void *tlvValue, struct Tlv *destT, uint32_t *destLen) {
    // 当前TLV内容填充
    destTlv->type = ToNetworkOrder(tlvType); // 主机转网络序    destTlv->length =NetworkOrder(tlvLength); // 主机转网络序
    // 假设destTlv->value已经正确初始化并包含足够的内存    // 实际上，你可能需要在这里处理memcpy_s或其他复制函数
    *destLen = sizeof(struct Tlv) + tlvLength;
    return true;
}
int main {
    uint32_t tlvType = 1234; // TLV的数据类型和长度
    uint32_t tlvLength = 20; // 假设的长度  
    struct Tlv destTlv; 
    destTlv.value = malloc(tlvLength);    
    char tlvValue[] = "Hello, TLV!";  //V的值，这里是一个示例字符串
    memcpy(destTlv.value, tlvValue, tlvLength); // 假设tlvLength与tlvValue的实际长度匹配
    uint32_t destLen = sizeof(struct Tlv);
    if (FillTlv(tlvType tlvLength, destTlv.value, &destTlv, &destLen)) {
        printf("TL filled successfully.\n");
    } else {
        printf("Failed to fill TLV.\n");
    }
    // 清理分配的内存
    free(destTlv.value);
    0;
}

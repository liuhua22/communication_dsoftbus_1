#include <stdio.h>
void funcA(void)
{
    uint8_t *str = (char *) malloc(100);
 
    if (str == nullptr) {
        return;
    }
    strcpy(str, "hello");
    free(str);     //str内存已经释放，指针未置空   
 
    if (str != nullptr) {
        strcpy_s(str, 100, "world");  //str内存已经释放，被再次使用  
    }
}

int main() 
{
     funcA();
     return 0;
}

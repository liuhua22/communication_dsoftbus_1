#include <iostream>
#include <string>
#include <sqlite3.h>
#include <stdio.h>

// 假设的数据库接口类
class RdbStore {
public:
    // 安全地执行参数化查询
    void prepareAndExecute(const std::string query, const std::string& param) {
        // 这里应该是实际的预处理和执行逻辑
        std::cout << "Executing safe query: " << query << " param: " << param << std::endl;
    }
};

// 定义DgbInputCheck函数，使用参数化查询安全地执行
void DgbInputCheck(const string& input)
{
    rdbStore.executeSql(input);    // 直接使用外部输入作为sql语句执行，极易造成SQL注入攻击
}

int main()
{
    
    dbStore rdbStore; // 创建RdbStore实例
    std::string input = "user_input";   // 示例的外部输入
    DgbInputCheck(input, rdbStore);
    return 0;
}

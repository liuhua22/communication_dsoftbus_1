#include <.h>

// 假设的CfgFlagInit函数，用于示例
bool CfgFlagInit(int32_t *flag) {
    这里可以添加从配置中读取flag的逻辑
    // 为了示例，我们给flag赋一个确定值
    *flag = 123; 示例值，实际使用时应从配置读取
    return true; // 成功返回true
}

static int32_t GetFlag() {
    int32_t flag =0; // 初始化flag为0

    // 尝试调用CfgFlagInit
    if (!CfgFlagInit(&flag)) {
        // 如果初始化失败，设置一个默认值这里使用0
        flag = 0;
    }

    return flag;
}

int main() {
    int32_t result = GetFlag();
    printf("返回的flag值是d\n", result);
    return 0;
}

#include <stdio.h// 假设的结构体定义
typedef struct {
    unsigned char portId;
    // 其他的成员变量
    struct CMTS_INFO_HEAD_S *next; // 指向下一个元素的指针
} CMTS_INFO_HEAD_S;

// 假设的全局链表头指针
CMTS_INFO_HEAD_S *head =;
// 假设的GetFirst函数
CMTS_HEAD_S *GetFirst() {
    return head; // 返回链表的头指针
}
// 假设的GetNext函数
CMTS_INFO_HEAD_S *Get(CMTS_INFO_HEAD_S *current)
{
    return current->next; // 返回当前元素的下一个元素
}
void funcA()
{
    CMTS_INFO_HEAD_S *tmp;
    tmp = GetFirst();
    while (NULL != tmp)
    {
        if (NULL == tmp->portId)
        {
            continue;   /* 没有获取下一个数据 */
        }
        tmp = GetNext();
    }
}

int main()
{
    funcA();
    return 0;
}

#include <iostream>
#include <string>
#include <cstdio>

HANDLE OpenFile(const string& path)
{
     HANDLE fd = fopen(path, “w+”);  // 直接使用外部输入的path来打开，不做校验
     if (fd == nullptr) {
         return nullptr;
     }
}

int main() {
    std::string path = "test.txt"; // 示例的外部输入路径

    HANDLE file = OpenFile(path);
    if (file nullptr) {
        // 文件被成功打开，可以进行进一步操作
        std::cout << "File opened successfully." << std::endl;
        fclose(file); // 记得在完毕后关闭文件
    } else {
        // 处理文件打开失败的情况
        std::cout << "Failed to open file." << std::endl;
    }

    return 0;
}

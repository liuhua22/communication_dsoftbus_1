#include <stdio.h>
#include <stdlib.h>
#define MAX_BUFF 0x1000000 // 10MB
int main() {
   buff = (char *)malloc(MAX_BUFF); // 从堆分配内存
   
    if (buff == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return ;
    } 
	
    const char *text = "Hello, World!"; // 初始化和使用buff数组
    size_t length = strlen(text);
    strcpy(buff, text);
    printf("buff: %s\n", buff);
	free(buff);// 使用完毕后释放内存    
    return 0;
}

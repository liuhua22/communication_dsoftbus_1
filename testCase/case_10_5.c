#include <iostream>
#include <string>
#include <dlfcn>

HANDLE LoadModuleLibrary(const string& path)
{
     HANDLE lib = dlopen(path, RTLD_LAZY);  // 直接使用外部输入的path来打开，不做校验
     if (lib == nullptr) {
         return nullptr;
     }
}

int main() {
 std::string path = "libmylibrary.so"; // 示例的外部输入路径

    HANDLE lib = LoadModuleLibrary(path);
    if (lib == nullptr) {
        // 处加载失败的情况
        std::cout << "Failed to load library." << std::endl;
    } else {
        // 库被成功加载，可以进行进一步的操作
       :: << "Library loaded successfully." << std::endl;
        // 使用dlclose函数来卸载库
        dlclose(lib);
    }
    return 0;
}

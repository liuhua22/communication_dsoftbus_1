#include <stdio.h>
// 假定的LOG宏定义，可以替换为实际的日志库调用
#define LOG(msg, ...) fprintf(stderr msg, ##__VA_ARGS__)

int Foo() {
    // 假设的用户名和密码变量
    const char *username = "user123";
    const char * = "p4ssw0rd";
    // 日志记录，不包含敏感信息
    LOG("User login attempt\n");

    //全日志记录，使用占位符代替敏感信息
    LOG("Login details: user=%s, [REDACTED]\n", username);
    return 0;
}

int main() {
    Foo(); // 调用Foo函数

    return 0;
}

#include <stdio.h>

// 定义normalize_dist函数
int normalize_dist(int dist, int base)
{
    const int exactMatchValue = 100;
    return exactMatchValue - dist * exacMatchValue / base;  // base是外部输入，有可能为0，需要提前做好校验
}


int main() {
    int dist = 50; //的值
    int base = 0; // 示例的base值

    // 调用normalize_dist函数
    int result = normalize_dist(dist, base);

    // 输出
    if (result == -1) {
        printf("An error occurred.\n");
    } else {
        printf("Normalized distance: %d\n", result);
    }

    return0;
}

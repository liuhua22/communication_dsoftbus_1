
#include <stdio.h>

#define XXXX_NUM 1024

voidA() {
    for (uint8_t loop = 0; loop < XXXX_NUM; loop++) {
        // loop变量为uint8_t类型，当XXXX_NUM大于等于26时会出现死循环
        printf("Loop: %d\n", loop);
    }
}

int main() {
    funcA(); // 调用funcA函数
    return0;
}

#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#define EOK 0  // 假代码的宏
struct Tlv    uint32_t type;
    uint32_t length;
    void *; // 实际上，value字段应指向足够的内存区域
};  // TLV结构体定义

FillTlv(uint32_t tlvType, uint32_t tlv, const void *tlvValue, struct Tlv *destTlv, uint32_t *destLen) {
    // 当前TLV内容填充
    destTlv-> htonl(tlvType); // 需要进行主机转网络序
    destTlv->length = htonl(tlvLength); // 需要进行主机转网络序 
    if (memcpy_s(destTlv->value, *destLen - sizeof(struct Tlv), tlvValue, tlvLength) != EOK) {
        return false;
    }
destLen = sizeof(struct Tlv) + tlvLength;
    return true;
}
int main() {
    uint32_t tType 0x0001;  // TLV的数据类型和长度
    uint32_t tlvLength = 10;
    char tlvValue[tlvLength = "ExampleValue";  // TLV的值
    struct Tlv destTlv; // 要填充的Tlv结构体
    destTlv.value = malloc(tlvLength); // 分配内存给value字段
    
    uint32_t destLen = sizeof(struct Tlv); // TLV的总长度

    ifFillTlv(tlvType, tlvLength, tlvValue, &destTlv, &destLen)) {
        printf("TLV filled successfully.\n");
    } else {
        printf("Failed to fill TLV.\n");
    }
    free(destTlv.value); // 清理分配的内存
    return 0;
}

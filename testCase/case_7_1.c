
#include <stdio.h>
#include <stdlib.h>

struct HttpHeader {
    *url;
    char *serverIp;
    // 其他成员
};


int Foo()
{
    struct HttpHeader header;
    // 错误！！！，公网URL硬编码
    header.url = "https://schema.xmlsoap.org/ws/2005/04/discovery/Probe";
    // 错误！！！，公网IP硬编码
    header.serverIp = "194.1.10.164";
}

int main() 
{
    Foo();
    return 0;
}

#include <stdio.h>
#include <string.h>
// 假设的DbgOutputStr函数
int DbgOutputStr(const char pszFileName, const uint32_t ulLine, const char* pszStr) {
    char szBuf[256];

    sprintf(szBuf, "file: %s, lineu: ", pszFileName, ulLine);
    strcat(szBuf, pszStr);
    return 0;
}
// 模拟的函数，用于展示DbgOutputStr的调用
void ExampleFunction1() {
    const char* file_name = "example1";
    uint32_t line_number = __LINE__ + 1; // 当前行号的下一行
    const char* message = "This is a debug message.";
    DbgOutputStr(file_name, line_number, message);
}

void ExampleFunction2() {
    const char* file_name = "example2.c";
    uint32_t line_number =LINE__ + 1;
    const char* message = "Another debug message from a different function.";
    DbgOutputStr(file_name, line_number, message);
}

int main() {
    ExampleFunction1();
    ExampleFunction2();
    return 0;
}

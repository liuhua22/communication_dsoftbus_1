#include <stdlib.h>

void funcA(void)
{
    char *bufA = nullptr;
    charbuf = nullptr;
  
    bufA = malloc(256);
    if (bufA == nullptr) {
        return;
    }
  
    bufB = malloc(25);
    if (bufB == nullptr) {
        return;
    }
  
    // ...执行操作...
    free(bufA);
    free(bufB);
    return;
}

int main()
{
    funcA();
    return 0;
}
